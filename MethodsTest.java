public class MethodsTest {
	public static void main(String[] args) {
		int x = 5;
		/*System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);*/
		
		/*System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);*/
		
		//methodTwoInputNoReturn(10, 11.5);
		
		/*int number = methodNoInputReturn();
		System.out.println(number);*/
		
		/*double squareRoot = sumSquareRoot(2, 7);
		System.out.println(squareRoot);*/
		
		/*String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());*/
		
		
		System.out.println(SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn() {
		System.out.println("I'm a method that takes no inputs and returns nothing");
		
		int x = 20;
		
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int variable) {
		System.out.println("Inside the method one input no return");
		variable -= 5;
		System.out.println(variable);
	}
	
	public static void methodTwoInputNoReturn(int numberInt, double numberDouble) {
		System.out.println(numberInt);
		System.out.println(numberDouble);
	}
	
	public static double sumSquareRoot(int x, int y) {
		double result = Math.sqrt(x+y);
		return result;
	}
	
	public static int methodNoInputReturn() {
		return 5;
	}
	
	
	
	
}