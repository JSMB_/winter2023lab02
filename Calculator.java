public class Calculator {
	public static int add(int x, int y) {
		int result = x+y;
		return result;
	}
	
	public static int subtract(int x, int y) {
		int result = x-y;
		return result;
	}
	
	public int multiply(int x, int y) {
		int result = x*y;
		return result;
	}
	
	public int divide(int x, int y) {
		int result = x/y;
		return result;
	}
}