import java.util.Scanner;

public class PartThree {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Calculator cal = new Calculator();
		
		System.out.println("Please  a number:");
		int x = sc.nextInt();
		System.out.println("Please  a second number:");
		int y = sc.nextInt();
		
		System.out.println("Addition Result: " + Calculator.add(x, y));
		System.out.println("Subtraction Result: " + Calculator.subtract(x, y));
		System.out.println("Multiplication Result: " + cal.multiply(x, y));
		System.out.println("Divition Result: " + cal.divide(x, y));
	}
}